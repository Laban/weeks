package hallborg.weeks.mocks

case class MockDate(date: String, weekNumber: Int)
